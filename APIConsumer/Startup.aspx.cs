﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Http;
using System.Text;
using System.Net.Http.Headers;
using Newtonsoft.Json;

namespace APIConsumer
{
    public partial class Startup : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ApiContact contact = CreateContactEntity();

            string contentJson = JsonConvert.SerializeObject(contact);

            string result = CreateContact(contentJson).ToString();
            lblText.InnerText = result + " Contact added/updated in Dotmailer";

        }

        public string GetContactbyEmail(string emailID)
        { 
            var url = "/v2/contacts/" + emailID;

            using (var httpClient = new HttpClient())
            {
                httpClient.BaseAddress = new Uri("https://api.dotmailer.com");
                var credentials = Encoding.ASCII.GetBytes("apiuser-b911ba818d0a@apiconnector.com:P@ssw0rd1");
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(credentials));
                var result = httpClient.GetAsync(url).Result;
                return result.IsSuccessStatusCode.ToString();
            }
        }

        private static ApiContact CreateContactEntity()
        {
            ApiContact contact = new ApiContact
            {
                Email = String.Format("rashmisengar86@gmail.com", Guid.NewGuid().ToString("N")),
                EmailType = ApiContactEmailTypes.Html,
                OptInType = ApiContactOptInTypes.Single,
                DataFields = new ApiContactData[]
                        {
                            new ApiContactData{Key = "Gender", Value = "Female"},
                            new ApiContactData{Key = "FullName", Value = "Rashmi Malav"},
                            new ApiContactData{Key = "FIRSTNAME", Value = "Rashmi"},
                            new ApiContactData{Key = "LASTNAME", Value = "Malav"},
                            new ApiContactData{Key = "POSTCODE", Value="L2526"}
                        }
            };

            return contact;
        }

        public bool CreateContact(string content)
        {
            var url = "/v2/address-books/6101496/contacts";

            using (var httpClient = new HttpClient())
            {
                httpClient.BaseAddress = new Uri("https://api.dotmailer.com");
                var credentials = Encoding.ASCII.GetBytes("apiuser-b911ba818d0a@apiconnector.com:P@ssw0rd1");
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(credentials));
                var result = httpClient.PostAsync(url, new StringContent(content, Encoding.UTF8, "application/json")).Result;
                return result.IsSuccessStatusCode;

            }
        }

    }


}