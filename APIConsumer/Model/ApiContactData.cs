﻿using System;

namespace APIConsumer
{
    internal class ApiContactData
    {
        public String Key { get; set; }

        public Object Value { get; set; }
    }
}
